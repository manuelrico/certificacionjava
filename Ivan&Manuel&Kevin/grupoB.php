<?php
require './misFunciones.php';
$mysqli = conectaFlashCard();

$consulta = $mysqli->query("SELECT * FROM FlashCards Where Grupo='Grupo B' ORDER BY RAND() LIMIT 0, 1000000000");
$num_filas = $consulta->num_rows;
$listaPreguntas = array();
for ($i = 0; $i < $num_filas; $i++) {
    $resultado = $consulta->fetch_array();
    $listaPreguntas[$i][0] = $resultado['IDFlashcard'];
    $listaPreguntas[$i][1] = $resultado['Grupo'];
    $listaPreguntas[$i][2] = $resultado['Pregunta'];
    $listaPreguntas[$i][3] = $resultado['Respuesta'];
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>FlashCard</title>
        <link rel ="stylesheet" href="css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Francois+One" rel="stylesheet">
    </head>
    <style>
        .letraDelGrupo{
            font-size: 55px;
            font-family: fantasy;
            color: blue;
            margin-top: 5%;
            margin-left:10%;
            margin-bottom: 7%;
        }
        .cajas{
            height: 350px;
            min-height: 350px;
            width: 400px;
            min-width: 400px;
            border: 5px solid black;
            border-radius: 20px 20px 20px 20px;
            -moz-border-radius: 20px 20px 20px 20px;
            -webkit-border-radius: 20px 20px 20px 20px;
            border: 5px solid black;
        }
        .bordeContainer{
            border: 1px solid black;
        }
        .fondoPagina{
            background-color: #2e6da4;
        }
        .fondoContainer{
            background-color: white;
        }
        .letraFlip{
            font-size: 35px;
            text-align: center;
        }
    </style>


    <body class="fondoPagina">
        <div class="container fondoContainer" style="margin-top:1%; background-color: white; box-shadow: 2px 2px 10px rgba(0,0,0,.05); border-radius: 15px">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <button class="btn btn-block btn-primary btn-lg" style="margin-top: 10%;" onclick="location.href = '/certificacionjava/Ivan&Manuel&Kevin/menuFlashCard.php'"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span></button>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2"></div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <p class="letraDelGrupo" style="margin-left:25%;">Grupo B</p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4"></div>
            </div>
        </div>
        <br>
        <div class="container bordeContainer fondoContainer" style="height: 60%; background-color: white; box-shadow: 2px 2px 10px rgba(0,0,0,.05); border-radius: 15px">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2">

                </div>
                <div class="col-lg-8 col-md-8 col-sm-8" style="margin-top: 1%;">
                    <div id="FlashCard">
                        <!--para que el flip se lleve a cabo hay que determinar en cada unos de los div
                        cual es el FRENTE "front" y cual es la vuelta "back"-->
                        <div id="cambiarFrontal" class=" front cajas letraFlip" > 
                            <?php
                            print_r($resultado[2]);
                            ?>

                        </div>
                        <div id="cambiarTrasero" class=" back cajas letraFlip">
                            <?php
                            print_r($resultado[3]);
                            ?>
                        </div>

                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2"></div>
            </div>
        </div>
        <div class="container fondoContainer" style="margin-top:1%; background-color: white; box-shadow: 2px 2px 10px rgba(0,0,0,.05); border-radius: 15px">
            <div class="row">

                <div class="col-lg-5 col-md-5 col-sm-5"></div>
                <div class="col-lg-2 col-md-2 col-sm-2" style="padding:1%;">
                    <button class="btn-lg btn-block btn-primary" onclick="siguiente();">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>

                    </button>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5"></div>

            </div>
        </div>
    </body>
    <script src="js/jquery-3.1.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.flip.js"></script>
    <script>
    //            var pregunta;
    //            
    //            $(document).ready(function(){
    //                cambiaPregunta();
    //                countdown();
    //           });

    //        function cambiaPregunta(){
    //            pregunta = Math.floor(Math.random() *;
    ////            $('#pregunta').html(arrayPreguntas[pregunta][2]);
    ////            

    //        }
    //hacemos un flip por cada div
                            //Cada flip tiene que tener un id distinto a cada caja
                            //primer DIV
                            $("#FlashCard").flip();

    </script>
    <script tytpe="text/javascript">
        function siguiente() {
            var _id = <?php echo $resultado[0]; ?>;
            var _grupo = '<?php echo $resultado['Grupo'] ?>';
            $("#cambiarFrontal").load("cargarPregunta.php",{
                id: _id,
                grupo: _grupo
            });
        }
    </script>
</html>

