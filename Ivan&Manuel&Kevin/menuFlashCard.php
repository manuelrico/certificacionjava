<html>
    <title>
        Flash Card
    </title>
    <head>
        <style>
            .logo{
                margin-top: 5%;
            }
            .letraBoton{
                text-align: center;
            }
        </style>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div>
            <nav class="navbar navbar-inverse navbar-fixed-top">
                <div class="container-fluid">
                    <!--                    <div class="navbar-header">
                                            <a class="navbar-brand" href="#">WebSiteName</a>
                                        </div>
                                        <ul class="nav navbar-nav">
                                            <li class="active"><a href="#">Home</a></li>
                                            <li><a href="#">Page 1</a></li>
                                            <li><a href="#">Page 2</a></li>
                                            <li><a href="#">Page 3</a></li>
                                        </ul>-->
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Registrate</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                    </ul>
                </div>
            </nav>
        </div>
        <div class="logo">
            <center><img src="imagenes/flashcards.png"></center>
        </div>
        <br>
        <br>
        
        <div class="container">

            <div class="row">
                
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <a href="grupoA.php" class="btn-lg btn-block btn-primary letraBoton">Grupo A</a>
                </div>
                
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <a href="grupoB.php" class="btn-lg btn-block btn-primary letraBoton">Grupo B</a>
                </div>
                
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <a href="aleatorio.php" class="btn-lg btn-block btn-primary letraBoton">Aleatorio</a>
                </div>
                
                
            </div>
            
        </div>

<!--     <th><a href="anadir.php" class="caja" target="_blank"> Añadir Usuario </a></th>
     <th><a href="borrar.php" target="_blank"> Borrar Usuario </a></th>
     <th><a href="menu_Admin.php"> Actualizar </a></th>-->
        <!--    </tr>-->
        <!--    </table>	-->
    </div>

</body>



</html>

